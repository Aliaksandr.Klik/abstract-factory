public class MetalFactory implements FurnitureFactory {
    @Override
    public Table createTable() {
        return new MetalTable();
    }
    @Override
    public Chair createChair() {
        return new MetalChair();
    }
}
