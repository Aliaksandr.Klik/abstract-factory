public interface FurnitureFactory {
    Table createTable();
    Chair createChair();
}
