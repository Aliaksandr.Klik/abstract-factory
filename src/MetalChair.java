public class MetalChair implements Chair {
    @Override
    public String getChair() {
        return "This is a metal chair.";
    }
}
