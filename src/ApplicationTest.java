import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ApplicationTest {
    @Test
    void metalFactoryTest() {
        Application metalFactory = new Application("metal");
        Table metalTable = metalFactory.createTable();
        Chair metalChair = metalFactory.createChair();
        assertEquals("This is a metal table.", metalTable.getTable());
        assertEquals("This is a metal chair.", metalChair.getChair());
    }

    @Test
    void woodenFactoryTest() {
        Application woodenFactory = new Application("wooden");
        Table woodenTable = woodenFactory.createTable();
        Chair woodenChair = woodenFactory.createChair();
        assertEquals("This is a wooden table.", woodenTable.getTable());
        assertEquals("This is a wooden chair.", woodenChair.getChair());
    }
}