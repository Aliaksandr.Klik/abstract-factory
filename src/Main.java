public class Main {
    public static void main(String[] args) {
        Application metalFactory = new Application("metal");
        Table metalTable = metalFactory.createTable();
        Chair metalChair = metalFactory.createChair();
        System.out.println(metalTable.getTable());
        System.out.println(metalChair.getChair());
        Application woodenFactory = new Application("wooden");
        Table woodenTable = woodenFactory.createTable();
        Chair woodenChair = woodenFactory.createChair();
        System.out.println(woodenTable.getTable());
        System.out.println(woodenChair.getChair());
    }
}