public class WoodenFactory implements FurnitureFactory {
    @Override
    public Table createTable() {
        return new WoodenTable();
    }
    @Override
    public Chair createChair() {
        return new WoodenChair();
    }
}
