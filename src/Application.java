public class Application {
    private final FurnitureFactory factory;
    public Application(String type) {
        if (type.equals("wooden")) {
            factory = new WoodenFactory();
        } else if (type.equals("metal")) {
            factory = new MetalFactory();
        } else {
            throw new IllegalArgumentException("Unknown type of factory");
        }
    }
    public Table createTable() {
        return factory.createTable();
    }
    public Chair createChair() {
        return factory.createChair();
    }
}
